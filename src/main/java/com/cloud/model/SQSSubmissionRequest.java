package com.cloud.model;

import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.annotation.JsonPropertyOrder;

@JsonPropertyOrder({"JobID", "Source", "Destination", "RecipeId", "EDL"})
public class SQSSubmissionRequest {
    @JsonProperty("JobID")
    private String jobId;
    @JsonProperty("Source")
    private String source;
    @JsonProperty("Destination")
    private String destination;
    @JsonProperty("RecipeId")
    private String recipeId;
    private String EDL;

    public String getJobId() {
        return jobId;
    }

    public void setJobId(String jobId) {
        this.jobId = jobId;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getRecipeId() {
        return recipeId;
    }

    public void setRecipeId(String recipeId) {
        this.recipeId = recipeId;
    }

    public String getEDL() {
        return EDL;
    }

    public void setEDL(String EDL) {
        this.EDL = EDL;
    }
}
