package com.cloud.aws;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.aws.autoconfigure.context.ContextRegionProviderAutoConfiguration;
import org.springframework.cloud.aws.autoconfigure.context.ContextStackAutoConfiguration;
import org.springframework.context.annotation.ComponentScan;

@SpringBootApplication(scanBasePackages = "com.cloud", exclude = ContextStackAutoConfiguration.class)
public class SqsQueueApplication {

	public static void main(String[] args) {
		SpringApplication.run(SqsQueueApplication.class, args);
	}

}
