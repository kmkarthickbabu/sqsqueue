package com.cloud.controller;

import java.sql.Time;
import java.time.Duration;
import java.util.HashMap;
import java.util.Map;

import com.amazonaws.services.sqs.model.MessageAttributeValue;
import com.amazonaws.services.sqs.model.SendMessageRequest;
import com.cloud.model.SQSSubmissionRequest;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.json.GsonJsonParser;
import org.springframework.cloud.aws.messaging.core.QueueMessagingTemplate;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.support.MessageBuilder;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequestMapping("/sqs")
public class SQSController {
	
	private static final Logger LOG = LoggerFactory.getLogger(SQSController.class);
	
	@Autowired
	private QueueMessagingTemplate queueMessagingTemplate;


	
	@Value("${cloud.aws.endpoint.uri}")
	private String endpoint;
	
	@GetMapping
	public void sendMessage() throws InterruptedException, JsonProcessingException {
		LOG.info("inside send message");

		SQSSubmissionRequest request = new SQSSubmissionRequest();
		request.setDestination("testDestination");
		request.setEDL("testEDL");
		request.setJobId("testJobId");
		request.setRecipeId("testRecipeId");
		request.setSource("testSource");

		ObjectMapper mapper = new ObjectMapper();
		String values = mapper.writeValueAsString(request);

		for(int i=0; i<5; i++) {
		queueMessagingTemplate.send(endpoint,MessageBuilder.withPayload(request).build());
		//LOG.info("destination channel--" + queueMessagingTemplate.receive(endpoint));
		Thread.sleep(Duration.ofSeconds(5).toMillis());
		}
	}


	
	

}
