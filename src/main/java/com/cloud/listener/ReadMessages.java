package com.cloud.listener;

import com.cloud.model.SQSSubmissionRequest;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.cloud.aws.messaging.listener.annotation.SqsListener;
import org.springframework.messaging.handler.annotation.MessageMapping;
import org.springframework.messaging.handler.annotation.Payload;
import org.springframework.stereotype.Component;

import java.util.Base64;

@Component
public class ReadMessages {
	
	private static final Logger LOG = LoggerFactory.getLogger(ReadMessages.class);
	//@SqsListener("access-services-job")
	@MessageMapping("access-services-job")
	public void getMessage(@Payload String msg) {
		LOG.info("message from queue -- " + msg);
        String base64Format = Base64.getEncoder().encodeToString("hello".getBytes());
        LOG.info("decoded message " + Base64.getDecoder().decode(base64Format));
        LOG.info("decoded message " + new String(Base64.getDecoder().decode(base64Format)));
		System.out.println(" ");
	}

}
